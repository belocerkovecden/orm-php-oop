<?php

namespace App\Models;

trait HasPriceExample
{
    protected $price;

    public function getPrice()
    {
        return $this->price;
    }
}