<?php

namespace App\Controllers;

use App\Controller;

class Article extends Controller
{

    // : bool указание типа возвращаемой функции
    // http://localhost/article.php?id=3&name=Vasya
    /*protected function access(): bool
    {
        return isset($_GET['name']) && 'Vasya' == $_GET['name'];
    }*/

    protected function handle()
    {
        $this->view->article = \App\Models\Article::findById($_GET['id']);
        echo $this->view->render(__DIR__ . '/../../templates/article.php');
    }
}