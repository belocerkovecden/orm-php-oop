<?php

namespace App;

class View
{
    protected $data = [];

    /*
     * Если пытаюсь записать в не существующее свойство объекта
     * */
    public function __set($name, $value) // Магия
    {
        $this->data[$name] = $value;
    }

    /*
     * Если пытаюсь получить не существующее свойство объекта
     * */
    public function __get($name) // Магия
    {
        return $this->data[$name] ?? null;
    }

    /*
     * Должен вернуть ответ(true или false) на вопрос, а есть ли такое свойство?
     * */
    public function __isset($name) // Магия
    {
        return isset($this->data[$name]);
    }

    public function display($template)
    {
        include $template;
    }

    public function render($template)
    {
        ob_start();
        include $template;
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    public function count($name)
    {
        return count($this->data);
    }
}