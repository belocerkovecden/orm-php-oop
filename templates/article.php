<?php
ini_set('display_errors', true);
error_reporting(E_ALL ^ E_NOTICE);
?>

<!doctype html>
<html lang=ru>
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
<div><a href="index.php">На главную</a></div>
<hr>
<h1><?php echo $this->article->title; ?></h1>
<span style="color: darkcyan">id: <?php echo $this->article->id; ?></span>
<hr>
  <article>
    <p><?php echo $this->article->content; ?></p>
  </article>

</body>
</html>