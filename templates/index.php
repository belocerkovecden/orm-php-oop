<!doctype html>
<html lang=ru>
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
<h1>Новости</h1>

<?php foreach ($this->articles as $article) : ?>
  <article>
    <h2>
      <a href="/?ctrl=Article&id=<?php echo $article->id ?>">
        <?php echo mb_substr($article->title, 0, 30); ?>
      </a>
    </h2>
    <p><?php echo mb_substr($article->content, 0, 70); ?></p>
  </article>
  <hr>
<?php endforeach; ?>

</body>
</html>